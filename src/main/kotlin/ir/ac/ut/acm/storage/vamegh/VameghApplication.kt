package ir.ac.ut.acm.storage.vamegh

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class VameghApplication

fun main(args: Array<String>) {
    runApplication<VameghApplication>(*args)
}
